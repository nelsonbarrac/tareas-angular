import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit, AfterViewInit {
  @ViewChild('deleteSwal') deleteSwal: SwalComponent;
  @ViewChild('successSwal') successSwal: SwalComponent;

  form: FormGroup;

  disabledButton = false;

  rbd = null;

  aEliminar = null;

  todos: any = {
    colaboradores: []
  };

  constructor(public fb: FormBuilder, public route: ActivatedRoute){
    this.form = fb.group({
      colaboradoresArray: fb.array([]),
      tareas: fb.array([])
    });
   }



ngOnInit(): void {
      const user = localStorage.getItem('user');
      const storedTasks = localStorage.getItem(user + '-tareas');

      if(!storedTasks){
        this.todos.colaboradores = [];
        this.todos.tareas = [];
      } else {
        const parsedStoredTasks = JSON.parse(storedTasks);
        this.todos.colaboradores = parsedStoredTasks.colaboradoresArray;
        this.todos.tareas = parsedStoredTasks.tareas;
      }

      const colaboradoresArray = this.form.get('colaboradoresArray') as FormArray;

      for (const e of this.todos.colaboradores){
        const item =  this.fb.group({
          abbr: [e.abbr, [Validators.required, Validators.maxLength(2)]],
          descripcion: [e.descripcion, [Validators.required]]
        });

        colaboradoresArray.push(item);
      }

      const tareas = this.form.get('tareas') as FormArray;

      for (const a of this.todos.tareas){

        const subtareas = [];

        for (const d of a.subtareas){

          const subtarea = this.fb.group({
            descripcion: [d.descripcion, Validators.required],
            open: d.open
          });

          subtareas.push(subtarea);
        }

        const item =  this.fb.group({
          descripcion: [a.descripcion, Validators.required],
          subtareas: this.fb.array(subtareas)
        });

        tareas.push(item);
      }
}

get colaboradoresArray(): FormArray{
  return this.form.get('colaboradoresArray') as FormArray;
}

get tareas(): FormArray{
  return this.form.get('tareas') as FormArray;
}

deleteColaboradores(i): void{
  const colaboradoresArray = this.form.get('colaboradoresArray') as FormArray;
  colaboradoresArray.removeAt(i);
}

createColaboradores(): FormGroup{
  return this.fb.group({
    abbr: ['', [Validators.required, Validators.maxLength(2)]],
    descripcion: ['', Validators.required]
  });
}

addColaboradores(): void{
  const colaboradoresArray = this.form.get('colaboradoresArray') as FormArray;
  colaboradoresArray.push(this.createColaboradores());
}

crearTarea(): FormGroup{
  return this.fb.group({
    descripcion: ['', Validators.required],
    subtareas: this.fb.array([])
  });
}

addTarea(): void{
  const tareas = this.form.get('tareas') as FormArray;
  tareas.push(this.crearTarea());
}

deleteTarea(): void{
  const tareas = this.form.get('tareas') as FormArray;
  tareas.removeAt(this.aEliminar);
}

confirmEliminar(i): void{
  this.aEliminar = i;
  const tareas = this.form.get('tareas') as FormArray;
  const desc = tareas.at(i).get('subtareas') as FormArray;
  const largo = desc.length;
  if (largo > 0){
    this.deleteSwal.fire();
  }
  else{
    this.deleteTarea();
  }
}

crearsubtarea(defaultValue?: any): FormGroup{
  if (defaultValue){
    return this.fb.group({
      descripcion: [defaultValue.descripcion, Validators.required],
      open: defaultValue.open
    });
  }
  else{
    return this.fb.group({
      descripcion: ['', Validators.required],
      open: false
    });
  }
}

addSubtarea(area, i): void{
  const subtareas = area.get('subtareas') as FormArray;
  const subtarea = this.crearsubtarea();
  subtareas.push(subtarea);
}

deleteSubtarea(area, j): void{
  const subtareas = area.get('subtareas') as FormArray;
  subtareas.removeAt(j);
}

submitTodos(): void{

  this.validateAllFormFields(this.form);
  if (!this.form.valid){
    return;
  }

  this.disabledButton = true;
  this.disabledButton = false;
  const user = localStorage.getItem('user');
  localStorage.setItem(user + '-tareas', JSON.stringify(this.form.value));
  this.successSwal.fire();
}

validateAllFormFields(formGroup: any): void{
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }else if (control instanceof FormArray){
      for (const c of control.controls){
        this.validateAllFormFields(c);
      }
    }
  });
}

ngAfterViewInit(): void {
 // $("#sticker").sticky({topSpacing:0});
}


}
