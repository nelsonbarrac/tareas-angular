import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TodoLayoutComponent } from '../todo-layout/todo-layout.component';
import { LoginComponent } from '../login/login.component';
import { AuthGuardService } from '../auth-guard.service';

const routes = [
  {path: 'login', component: LoginComponent},
  {path: 'todo', component: TodoLayoutComponent, canActivate: [AuthGuardService]},
  {path: '', redirectTo: 'todo', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
