import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(public fb: FormBuilder, public router: Router) {
    this.form = fb.group({
      username: ['', [Validators.required, Validators.email]],
    });
  }

  login(): void{
      const mail = this.form.get('username').value;
      localStorage.setItem('user', mail);
      this.router.navigate(['todo']);
  }

  ngOnInit(): void {
  }

}
